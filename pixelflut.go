package main

import (
	"fmt"
	"image"
	"net"
	"os"
	"strconv"
	"time"

	_ "image/png"
)

const X_SIZE = 800;
const Y_SIZE = 600;

const CORES = 10
const PIXELS_PER_CORE = (X_SIZE * Y_SIZE) / CORES

func main() {
	pixel := 0

	for x := 0; x < X_SIZE; x++ {
		for y := 0; y < Y_SIZE; y++ {
			if pixel == 0 {
				go spam(x, y)
				pixel = PIXELS_PER_CORE
			}

			pixel--
		}
	}

	for {
		time.Sleep(1000)
		continue
	}
}

func spam(startX int, startY int) {
	pixels := getPixels(startX, startY)

	conn, _ := net.Dial("tcp", "localhost:1337")

	for {
		for _, b := range pixels {
			_, _ = conn.Write(b)
		}
	}
}

func getPixels(startX int, startY int) [PIXELS_PER_CORE][]byte  {
	var pixels [PIXELS_PER_CORE][]byte

	flag := getFlag()

	pixel := 0

	for x := startX; x < X_SIZE; x++ {
		for y := startY; y < Y_SIZE; y++ {
			r, g, b, _ := flag.At(x, y).RGBA()

			r, g, b = r/0x101, g/0x101, b/0x101

			hex := fmt.Sprintf("%02x%02x%02x", r, g, b)

			pixels[pixel] = []byte("PX " + strconv.Itoa(x) + " " + strconv.Itoa(y) + " " + hex + "\n")
			pixel++

			if (pixel >= PIXELS_PER_CORE) {
				break;
			}
		}

		if (pixel >= PIXELS_PER_CORE) {
			break;
		}
	}

	return pixels
}

func getFlag() image.Image {
	reader, _ := os.Open("flag.png")
	defer reader.Close()
	im, _, _ := image.Decode(reader)

	return im
}